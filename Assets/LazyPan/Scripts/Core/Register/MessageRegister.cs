using System;
using System.Collections.Generic;

namespace LazyPan {
    public class MessageRegister : Singleton<MessageRegister> {
        private MsgRegister register = new MsgRegister();

        public void Reg(int id, Action a) {
            register.Register(id, a);
        }

        public void Reg<T>(int id, Action<T> a) {
            register.Register(id, a);
        }

        public void Reg<T1, T2>(int id, Action<T1, T2> a) {
            register.Register(id, a);
        }

        public void UnReg(int id, Action act) {
            register.UnRegister(id, act);
        }

        public void UnReg<T>(int id, Action<T> act) {
            register.UnRegister(id, act);
        }

        public void UnReg<T1, T2>(int id, Action<T1, T2> act) {
            register.UnRegister(id, act);
        }

        public void Dis(int id) {
            register.Dispatcher(id);
        }

        public void Dis<T>(int id, T t) {
            register.Dispatcher(id, t);
        }

        public void Dis<T1, T2>(int id, T1 t1, T2 t2) {
            register.Dispatcher(id, t1, t2);
        }
    }

    public class Body {
        public int id;
        public Delegate handler;

        public Body(int id, Delegate handler) {
            this.id = id;
            this.handler = handler;
        }

        public void Invoke() {
            ((Action)handler).Invoke();
        }

        public void Invoke<T>(T t) {
            ((Action<T>)handler).Invoke(t);
        }

        public void Invoke<T1, T2>(T1 t1, T2 t2) {
            ((Action<T1, T2>)handler).Invoke(t1, t2);
        }

        public void Invoke<T1, T2, T3>(T1 t1, T2 t2, T3 t3) {
            ((Action<T1, T2, T3>)handler).Invoke(t1, t2, t3);
        }
    }

    public class MsgRegister {
        private Dictionary<int, List<Body>> handles = new Dictionary<int, List<Body>>();

        public void Register(int id, Delegate e) {
            List<Body> acts;
            if (!handles.TryGetValue(id, out acts)) {
                acts = new List<Body>();
                handles.Add(id, acts);
            }

            if (SearchWrapperIndex(acts, e) == -1) {
                acts.Add(new Body(id, e));
            }
        }

        private int SearchWrapperIndex(List<Body> wps, Delegate handler) {
            int length = wps.Count;
            for (int i = 0; i < length; ++i) {
                if (wps[i].handler == handler) {
                    return i;
                }
            }

            return -1;
        }

        public void UnRegister(int id, Delegate handler) {
            if (handler == null) {
                return;
            }

            List<Body> acts;
            if (handles.TryGetValue(id, out acts)) {
                int index = SearchWrapperIndex(acts, handler);
                if (index >= 0) {
                    acts.RemoveAt(index);
                    handles[id] = acts;
                }
            }
        }

        public void Dispatcher(int id) {
            if (handles.TryGetValue(id, out var acts)) {
                for (int i = 0; i < acts.Count; i++) {
                    acts[i].Invoke();
                }
            }
        }

        public void Dispatcher<T>(int id, T act) {
            if (handles.TryGetValue(id, out List<Body> acts)) {
                for (int i = 0; i < acts.Count; i++) {
                    acts[i].Invoke(act);
                }
            }
        }

        public void Dispatcher<T1, T2>(int id, T1 act1, T2 act2) {
            if (handles.TryGetValue(id, out List<Body> acts)) {
                for (int i = 0; i < acts.Count; i++) {
                    acts[i].Invoke(act1, act2);
                }
            }
        }
    }

    public class MessageCode {
        public static int GameOver = 9;//游戏结束
        public static int RecycleActivable = 8;//回收可激活
        public static int BeInjuried = 6;//被受伤
        public static int BeSelfDetonation = 2;//自爆
        public static int DeathDrop = 0;//死亡掉落
        public static int PlayerUpgrade = 7;//玩家升级
        public static int LevelUpgrade = 3; //关卡升级
        public static int LevelUpgradeIncreaseRobot = 5;//关卡升级新增怪物
        public static int RobotCreate = 4;//生成怪物
        public static int BeHit = 1;//被击中
    }
}